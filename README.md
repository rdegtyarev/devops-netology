# devops-netology

Игнорировать папки terraform
**/.terraform/*

Игнорировать файлы с расширением  "tfstate" и содержащие в имени ".tfstate."
*.tfstate
*.tfstate.*

Игнорировать файл:
crash.log

Игнорировать файлы с расширением "ftvars"
*.tfvars

Игнорировать файлы:
override.tf
override.tf.json

Игнорировать все файлы, заканчивающиеся на "_override.tf" и "_override.tf.json"
*_override.tf
*_override.tf.json

Игнорировать файлы:
.terraformrc
terraform.rc